require 'test_helper'

class ListNewTest < ActionDispatch::IntegrationTest

  test "invalid list created" do
    get new_list_path
    assert_no_difference 'List.count' do
      post lists_path, params: { list: { name: "" } }
    end
    assert_template 'lists/new'
  end

  test "valid list created" do
    get new_list_path
    assert_difference 'List.count', 1 do
      post lists_path, params: { list: { name: "買い物リスト" } }
    end
    follow_redirect!
    assert_template 'lists/new'
  end
end
