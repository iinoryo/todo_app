require 'test_helper'

class PostListsTest < ActionDispatch::IntegrationTest
  include ApplicationHelper

  def setup
    @list = lists(:shopping)
  end

  test "post display" do
    get list_path(@list)
    assert_template 'lists/show'
    assert_select 'title', full_title(@list.name)
    assert_select 'h1', text: @list.name
    assert_select 'div.pagination'
    @list.posts.paginate(page: 1).each do |post|
      assert_match post.content, response.body
    end
  end
end
