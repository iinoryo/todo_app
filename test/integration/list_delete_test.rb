require 'test_helper'

class ListDeleteTest < ActionDispatch::IntegrationTest

  def setup
    @list = lists(:shopping)
    @post = posts(:orange)
  end
  
  test "delete list" do
    post lists_path, params: { list: { name: "テスト用リスト" } }
    get new_list_path
    assert_template 'lists/new'
    assert_select 'img', alt: 'Garbage'
    new_list = List.last
    assert_difference 'List.count', -1 do
      delete list_path(new_list)
    end
  end
end
