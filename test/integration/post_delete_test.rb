require 'test_helper'

class PostDeleteTest < ActionDispatch::IntegrationTest

  def setup
    @list = lists(:shopping)
    @post = posts(:orange)
  end
  
  test "delete post" do
    get list_path(@list)
    assert_template 'lists/show'
    assert_select 'img', alt: 'Garbage'
    assert_difference 'Post.count', -1 do
      delete post_path(@post)
    end
  end
end
