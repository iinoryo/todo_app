require 'test_helper'

class StaticPagesControllerTest < ActionDispatch::IntegrationTest

  def setup
    @base_title = "ToDoApp"
  end

  test "should get root" do
    get root_url
    assert_response :success
  end

  test "should get home" do
    get root_url
    assert_response :success
    assert_select "title", "ToDoApp"
  end

  test "should get lists" do
    get new_list_path
    assert_response :success
  end

end
