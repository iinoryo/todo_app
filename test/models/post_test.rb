require 'test_helper'

class PostTest < ActiveSupport::TestCase

  def setup
    @list = lists(:shopping)
    @post = @list.posts.build(content: "ピーマン", expiration_date: "2018-05-19 13:55:52",
                                                           list_id: @list.id)
  end

  test "should be valid" do
    assert @post.valid?
  end

  test "content should be present" do
    post_not_content = @list.posts.create(content: "   ")
    assert_not post_not_content.valid?
  end

  test "content should be at most 30 characters" do
    @post.content = "a" * 31
    assert_not @post.valid?
  end

  test "order should be most recent first" do
    assert_equal posts(:most_recent), Post.first
  end
end
