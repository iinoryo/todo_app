require 'test_helper'

class ListTest < ActiveSupport::TestCase
  def setup
    @list = lists(:shopping)
  end

  test "should be valid" do
    assert @list.valid?
  end

  test "name should be present" do
    list2 = List.new(name: "")
    assert_not list2.valid?
  end

  test "name should not be too long" do
    list3 = List.new(name: "a" * 31)
    assert_not list3.valid?
  end
end
