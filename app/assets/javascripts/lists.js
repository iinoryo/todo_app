$(function() {
  $(document).on('ajax:complete', '.list_delete', function(event, ajax, status) {
    var response;
    response = $.parseJSON(ajax.responseText);
    $('#list-'+response.list.id).hide(1500);
  });
});

$(function() {
  $('.comp-btn').click(function() {
    var status = $(this).attr('id');

    if (status === 'complete') {
      $(this).val('未完了')
        .removeClass('btn-success active')
        .addClass('btn-danger')
        .attr('id', 'incomplete');
    } else {
      $(this).val('完了')
        .removeClass('btn-danger')
        .addClass('btn-success active')
        .attr('id', 'complete');
    }
  });
});