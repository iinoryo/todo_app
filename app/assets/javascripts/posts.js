$(function() {
  $(document).on('ajax:complete', '.post_delete', function(event, ajax, status) {
    var response;
    response = $.parseJSON(ajax.responseText);
    $('#post-'+response.post.id).hide(1500);
  });
});