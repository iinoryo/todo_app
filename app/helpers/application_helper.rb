module ApplicationHelper

  # ページごとの完全なタイトルを返す
  # @param [String] page_title 各ページのタイトル
  # @return [String] ページタイトルと「ToDoApp」を連結した文字列
  # @return [String] ページタイトルが存在しない場合は「ToDoApp」の文字列のみ
  def full_title(page_title = '')
    base_title = 'ToDoApp'
    if page_title.blank?
      base_title
    else
      "#{page_title} | #{base_title}"
    end
  end
    
  # 日本式の日付に変換
  # @param [TimeWithZone] date 日付
  # @return [String] 日付を変換した文字列
  # @return [nil] TimeWithZone型以外の場合
  def convert_jp_date(date)
    date.strftime('%Y年 %m月 %d日') if date.kind_of?(ActiveSupport::TimeWithZone)
  end
end
