class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  include ListsHelper

  # リスト有効期限の更新
  def list_expiration_date
    date = @list.first_Incomplete_expiration_date
    @list.update_attributes(expiration_date: date)
  end
end