class PostsController < ApplicationController

  # TODO作成時のアクション
  def create
    @list = List.find(post_params[:list_id])
    @post = @list.posts.build(post_params)
    @posts = @list.posts.paginate(page: params[:page])
    if @post.save
      # リスト有効期限を更新
      list_expiration_date
      flash[:success] = '新しいTODOが作成されました。'
      redirect_to "/lists/#{ post_params[:list_id] }"
    else
      render 'lists/show'
    end
  end
  
  # TODO削除時のアクション
  def destroy
    @post = Post.find(params[:id])
    @list = List.find(@post.list_id)
    @post.destroy
    list_expiration_date
    render :json => {:post => @post}
  end

  private
  
  # リストのパラメータ
  def post_params
    params.require(:post).permit(:list_id, :content, :expiration_date)
  end
end
