class ListsController < ApplicationController

  # リスト一覧表示時に一覧を生成するアクション
  def new
    @list = List.new
    @lists = List.paginate(page: params[:page])
  end

  # ポスト一覧表示時に一覧を生成するアクション
  def show
    @list = List.find(params[:id])
    @post = @list.posts.build
    @posts = @list.posts.paginate(page: params[:page])
  end
  
  # リスト作成時のアクション
  def create
    @list = List.new(list_params)
    @lists = List.paginate(page: params[:page])
    if @list.save
      flash[:success] = '新しいリストが作成されました。'
      redirect_to new_list_path
    else
      render :new
    end
  end
  
  # リスト削除時のアクション
  def destroy
    @list = List.find(params[:id])
    @list.destroy
    render :json => {:list => @list}
  end

  # 検索一覧を生成するアクション
  def search
    @lists = List.search(params[:search])
    @posts = Post.search(params[:search])
    @search_total = @lists.count + @posts.count
  end

  # TODOの完了フラグを切り替えるアクション
  def toggle_action
    @post = Post.find(params[:id])
    @list = List.find(@post.list_id)
    @post.complete_flg = @post.complete_flg == 0
    @post.save
    # リストの有効期限を更新
    list_expiration_date
  end

  private

  # リストのパラメータ
  def list_params
    params.require(:list).permit(:name)
  end
end
