class Post < ApplicationRecord
  belongs_to :list
  default_scope -> { order(created_at: :desc) }
  validates :content, presence: true, uniqueness: { scope: [:list_id] }, length: { maximum: 30 }
  validates :expiration_date, presence: true

  # 検索ワードを元にTODO名からTODOを検索する
  # @param [String] search 検索ワード
  # @return [List] 検索ワードに一致したTODOを返す
  # @return [List] 検索ワードに一致しない場合は全てのTODOを返す
  def self.search(search)
    if search
      Post.where(['content LIKE ?', "%#{search}%"])
    else
      Post.all
    end
  end
end
