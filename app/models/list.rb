class List < ApplicationRecord
  has_many :posts, dependent: :destroy
  default_scope -> { order(updated_at: :desc) }
  validates :name, presence: true, length: { maximum: 30 }, uniqueness: true

  # 期限が一番早い未完了のTODOの日付を取り出す
  # @return [Date] 期限が一番早い未完了のTODOの日付
  def first_Incomplete_expiration_date
    posts.unscope(:order).order(:expiration_date).find_by(complete_flg: 0).expiration_date
  end

  # 検索ワードを元にリスト名からリストを検索する
  # @param [String] search 検索ワード
  # @return [List] 検索ワードに一致したリストを返す
  # @return [List] 検索ワードに一致しない場合は全てのリストを返す
  def self.search(search)
    if search
      List.where(['name LIKE ?', "%#{search}%"])
    else
      List.all
    end
  end
end
