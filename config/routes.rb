Rails.application.routes.draw do
  root 'static_pages#home'
  
  resources :posts, only: [:create, :destroy]
  
  resources :lists do
    collection do
      match 'search', via: [:get, :post]
      post 'toggle_action'
    end  
  end
end
