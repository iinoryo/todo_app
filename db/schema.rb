# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170528080007) do

  create_table "lists", force: :cascade do |t|
    t.string   "name",                          comment: "リスト名"
    t.datetime "created_at",      null: false,  comment: "作成日"
    t.datetime "updated_at",      null: false,  comment: "更新日"
    t.datetime "expiration_date",               comment: "期限日"
  end

  create_table "posts", force: :cascade do |t|
    t.text     "content",                       comment: "TODO名"
    t.datetime "expiration_date",               comment: "期限日"
    t.integer  "complete_flg",    default: 0,   comment: "完了フラグ"
    t.integer  "list_id",                       comment: "親リストID"
    t.datetime "created_at",      null: false,  comment: "作成日"
    t.datetime "updated_at",      null: false,  comment: "更新日"
    t.index ["list_id", "created_at"], name: "index_posts_on_list_id_and_created_at"
    t.index ["list_id"], name: "index_posts_on_list_id"
  end

end
