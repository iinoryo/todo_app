List.create!(name: "買い物リスト")

Faker::Config.locale = :ja

99.times do |n|
  name = Faker::Name.unique.name
  List.create!(name: name)
end

lists = List.order(:created_at).take(6)
50.times do
  content = Faker::Pokemon.unique.name
  flg = rand(0..1)
  date = Faker::Date.forward(100)
  created_date = Faker::Date.backward(100)
  lists.each { |list| list.posts.create!(content: content,
                                         complete_flg: flg,
                                         expiration_date: date,
                                         created_at: created_date) }
end