class CreateLists < ActiveRecord::Migration[5.0]
  def change
    create_table :lists do |t|
      t.string :name, comment: "リスト名"

      t.timestamps
    end
  end
end
