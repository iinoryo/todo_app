class AddExpirationDateToLists < ActiveRecord::Migration[5.0]
  def change
    add_column :lists, :expiration_date, :datetime, comment: "期限日"
  end
end
