class CreatePosts < ActiveRecord::Migration[5.0]
  def change
    create_table :posts do |t|
      t.text :content,                        comment: "TODO名"
      t.datetime :expiration_date,            comment: "期限日"
      t.integer :complete_flg,                comment: "完了フラグ"
      t.references :list, foreign_key: true

      t.timestamps
    end
    add_index :posts, [:list_id, :created_at]
  end
end
