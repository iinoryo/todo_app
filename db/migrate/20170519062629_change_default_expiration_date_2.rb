class ChangeDefaultExpirationDate2 < ActiveRecord::Migration[5.0]
  def change
    change_column_default :posts, :complete_flg, 0, comment: "完了フラグ"
  end
end
